import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DisponibilidadesService } from './disponibilidades.service';

import { AppComponent } from './app.component';
import { DisponibilidadesComponent } from './disponibilidades/disponibilidades.component';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import {ChartModule} from 'primeng/chart';
import { BarChartCountDisponibilidadesComponent } from './bar-chart-count-disponibilidades/bar-chart-count-disponibilidades.component';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
  declarations: [
    AppComponent,
    DisponibilidadesComponent,
    BarChartCountDisponibilidadesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TableModule,
    ChartModule,
    TooltipModule
  ],
  providers: [ DisponibilidadesService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
