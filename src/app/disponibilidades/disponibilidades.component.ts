import { Component, OnInit } from '@angular/core';
import { DisponibilidadesService } from '../disponibilidades.service';
import { Disponibilidades } from '../disponibilidades.model';
import { CountDisponibilidades } from '../countdisponibilidades.model';

@Component({
  selector: 'app-disponibilidades',
  templateUrl: './disponibilidades.component.html',
  styleUrls: ['./disponibilidades.component.sass']
})
export class DisponibilidadesComponent implements OnInit {

  disponibilidades: Disponibilidades[] = [];
  

  constructor(private disponibilidadesService: DisponibilidadesService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData(): void {
    this.disponibilidadesService.getDisponibilidades()
      .then((res: Disponibilidades[]) => {
        this.disponibilidades = res;
      }, (error: any) => {
        console.log(error)
      });
  }

  
 
}
