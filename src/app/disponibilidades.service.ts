import { Disponibilidades } from './disponibilidades.model';
import { CountDisponibilidades } from './countdisponibilidades.model';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_API } from './app.api';
import { ErrorHandler } from './app.error-handler';



@Injectable({
  providedIn: 'root'
})
export class DisponibilidadesService {

  constructor(private http: HttpClient) { }

  getDisponibilidades(): any {
    return this.http.get(`${URL_API}`)
    .toPromise()
    .then(res => res['content'] as Disponibilidades[]);
  }

  getCountDisponibilidades(version?: string): any {
    let url = `${URL_API}/countDisponibilidades`;
    if (version === '3.10' || version === '4.00') {
      url = `${URL_API}/countDisponibilidades?version=${version}`;
    }

    return this.http.get(url)
    .toPromise()
    .then(res => {
      return res as CountDisponibilidades[]
    });
  }


}
