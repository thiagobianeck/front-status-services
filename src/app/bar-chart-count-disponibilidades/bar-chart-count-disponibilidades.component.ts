import { Component, OnInit } from '@angular/core';
import { DisponibilidadesService } from '../disponibilidades.service';
import { CountDisponibilidades } from '../countdisponibilidades.model';

@Component({
  selector: 'app-bar-chart-count-disponibilidades',
  templateUrl: './bar-chart-count-disponibilidades.component.html',
  styleUrls: ['./bar-chart-count-disponibilidades.component.sass']
})
export class BarChartCountDisponibilidadesComponent implements OnInit {

  countDisponibilidadesv310: CountDisponibilidades[] = [];
  countDisponibilidadesv400: CountDisponibilidades[] = [];
  data310: any;
  data400: any;
  

  constructor(private disponibilidadesService: DisponibilidadesService) { }

  ngOnInit() {
    this.loadCountDisponibilidades("3.10");
    this.loadCountDisponibilidades("4.00");
  }

  loadCountDisponibilidades(versionLc: string): any {
    
    let countDisponibilidades: CountDisponibilidades[] = [];
    this.disponibilidadesService.getCountDisponibilidades(versionLc)
      .then((res: CountDisponibilidades[]) => {
        countDisponibilidades = res;
        if (versionLc === '3.10') {
          this.data310 = this.montaDados(countDisponibilidades);
        } else {
          this.data400 = this.montaDados(countDisponibilidades);
        }
      }, (error: any) => {
        console.log(error)
      });

  }

  montaDados(dados: CountDisponibilidades[]): void {

    let labels: string[] = [];
    let disponiveis: number[] = [];
    let indisponiveis: number[] = [];
    let data: any;

    dados.forEach(element => {
      labels.push(element.uf);
      disponiveis.push(element.disponibilidades);
      indisponiveis.push(element.indisponibilidades);
      data = this.montaGrafico(labels, disponiveis, indisponiveis);
    });

    return data;
  }

  montaGrafico(labels: string[], disponiveis: number[], indisponiveis: number[]): any {
    return {
      labels: labels,
      datasets: [
        {
          label: 'Disponíveis',
          backgroundColor: '#238E23',
          borderColor: '#238E23',
          data: disponiveis
        },
        {
          label: 'Indisponíveis',
          backgroundColor: '#FF0000',
          borderColor: '#FF0000',
          data: indisponiveis
        }
      ]
    };
  }

  

}
