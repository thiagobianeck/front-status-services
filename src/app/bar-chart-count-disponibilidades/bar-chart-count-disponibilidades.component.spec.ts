import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarChartCountDisponibilidadesComponent } from './bar-chart-count-disponibilidades.component';

describe('BarChartCountDisponibilidadesComponent', () => {
  let component: BarChartCountDisponibilidadesComponent;
  let fixture: ComponentFixture<BarChartCountDisponibilidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarChartCountDisponibilidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartCountDisponibilidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
