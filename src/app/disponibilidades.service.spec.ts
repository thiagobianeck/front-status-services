import { TestBed } from '@angular/core/testing';

import { DisponibilidadesService } from './disponibilidades.service';

describe('DisponibilidadesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DisponibilidadesService = TestBed.get(DisponibilidadesService);
    expect(service).toBeTruthy();
  });
});
