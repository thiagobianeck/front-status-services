export interface CountDisponibilidades {
  uf: string;
  versaoWebService: string;
  disponibilidades: number;
  indisponibilidades: number;
}