export interface Disponibilidades {
  id: number;
  uf: string;
  statusAutorizacao: boolean;
  statusRetornoAutorizacao: boolean;
  statusInutilizacao: boolean;
  statusConsultaProtocolo: boolean;
  statusStatusServico: boolean;
  statusConsultaCadastro: boolean;
  statusRecepcaoEvento: boolean;
  versaoWebService: string;
  dataConsulta: string;
}